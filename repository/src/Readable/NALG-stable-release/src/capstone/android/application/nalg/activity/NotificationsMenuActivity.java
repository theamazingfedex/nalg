package capstone.android.application.nalg.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import capstone.android.application.nalg.R;

public class NotificationsMenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifications_menu);
		setRequestedOrientation(1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notifications_menu, menu);
		return true;
	}
	
	public void viewEvent(View view)
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		
		startActivity(intent);
	}
	public void goHome(View view)
	{
		Intent intent = new Intent(this, DashboardActivity.class);
		
		startActivity(intent);
	}
}
