package capstone.android.application.nalg;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import capstone.android.application.nalg.activity.EventTaskViewerActivity;
import capstone.android.application.nalg.activity.EventViewerActivity;
import capstone.android.application.nalg.activity.SubtaskViewerActivity;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.fragment.DatePickerFragment;
import capstone.android.application.nalg.fragment.TimePickerFragment;
import capstone.android.application.nalg.model.Event;
import capstone.android.application.nalg.model.Profile;
import capstone.android.application.nalg.model.Task;

public class SubtaskManagerActivity extends Activity {
	int currentParentId = -1;
	int currentTaskId = -1;
	int currentEventId = -1;
	Event currentEvent = null;
	Task currentTask = null;
	GsonHandler gson = null;
	String androidId = null;
	ArrayList<Task> currentSubtasks = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_subtask_manager);
		setRequestedOrientation(1);
		
		gson = new GsonHandler(getApplicationContext());
		
		androidId = getIntent().getExtras().getString("CURRENT_USER_ID");
		currentParentId = getIntent().getExtras().getInt("CURRENT_PARENT_ID");
		currentTaskId = getIntent().getExtras().getInt("CURRENT_TASK_ID");
		currentEventId = getIntent().getExtras().getInt("CURRENT_EVENT_ID");
		currentEvent = gson.getEvent(currentEventId);
		
		if (currentTaskId == -1)
		{
			currentSubtasks = new ArrayList<Task>();
			currentTask = new Task();
		}
		else 
		{
			currentTask = gson.getTask(currentTaskId, currentEventId);
			currentSubtasks = currentTask.getSubtasks();
		}
		
		
		
		//build event task list viewer
		
		EditText dateView = ((EditText)findViewById(R.id.editNewEventDate));
		dateView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showDateDialog(v);
			}
		});
		EditText timeView = ((EditText)findViewById(R.id.editNewEventTime));
		timeView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTimeDialog(v);
			}
		});
		if (currentTask != null)
			populateDataFields();
	}
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
//	    	Intent intent = new Intent(this, EventViewerActivity.class);
//	    	intent.putExtra("CURRENT_EVENT_ID", currentTask.getEventId());
//	    	intent.putExtra("CURRENT_USER_ID", androidId);
//			startActivity(intent);
//	        return true;
//	    }
//	    return super.onKeyDown(keyCode, event);
//	}
	
	private void populateDataFields()
	{
		//split into separate declarations for debugging
		TextView taskName = ((TextView) findViewById(R.id.taskManager_field_taskName));
		taskName.setText(
				currentTask.getName());
		((TextView) findViewById(R.id.taskManager_field_description)).setText(currentTask.getDescription());
		((TextView) findViewById(R.id.editNewEventDate)).setText(currentTask.getDeadlineString());
		((TextView) findViewById(R.id.editEvent_hidden_dateInMillis)).setText(String.valueOf(currentTask.getDeadline()));
	}
	
	public void showDateDialog(View view) {
		DatePickerFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}
	public void showTimeDialog(View view){
		TimePickerFragment newFragment = new TimePickerFragment();
	    newFragment.show(getFragmentManager(), "timePicker");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.subtask_manager, menu);
		return true;
	}
	public void returnToEvent(View view)
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}
	public void saveTask(View view)
	{
		Intent intent = new Intent(this, SubtaskViewerActivity.class);
		
		Task parent = gson.getTask(currentParentId, currentEventId);
		currentTask = buildCurrentTask();
		Task tallestTask = gson.addSubtask(currentTask, parent, currentEventId);
		currentEvent = gson.addTaskToEvent(tallestTask, currentEvent);
			
			
//			ArrayList<Task> subtasks = parent.getSubtasks();
//			for (int i = 0; i < subtasks.size(); i++)
//			{
//				if (subtasks.get(i).getId() == currentTaskId);
//				{
//					subtasks.remove(i);
//					break;
//				}
//			}
//			currentTask = buildCurrentTask();
//			subtasks.add(currentTask);
//			// currentTask == 2
//			Task superParent = currentTask;// == 1
//			Task lastParentTask = null;
//			do {
//				superParent = gson.getParent(superParent, currentEvent);
//				if (superParent == currentTask)
//					break;
//				//save parent with currentTask
//				if (superParent != null){
//					currentTask = superParent;
//					Task tempParent = gson.getParent(superParent, currentEvent);
//					if (tempParent != null){
//						superParent = gson.updateSubtask(currentTask, tempParent);
//					}
//					lastParentTask = superParent;
//				}
//			} while(superParent != null);
//			
//			if (lastParentTask != null)
//				currentEvent = gson.addTaskToEvent(lastParentTask, currentEvent);
//			
//			gson.updateEvent(currentEvent);
			
			
////		while (currentEventId != currentParentId && currentParentId != -1){
//			if (superTask != null)
//				currentParentId = superTask.getId();
//			else {
//				currentParentId = currentEventId;
//				Event e = gson.addTaskToEvent(buildCurrentTask(), currentEvent);
//				gson.updateEvent(e);
////				break;
//			}
////		}
		
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_PARENT_ID", 
				gson.getParent(
							gson.getTask(currentParentId, currentEventId), currentEventId)
							.getId()
						);
		intent.putExtra("CURRENT_TASK_ID", currentParentId);
		startActivity(intent);
	}
	public void launchSubtasks(View view){
		Intent intent = new Intent(this, SubtaskViewerActivity.class);
		
		Task parent = gson.getTask(currentParentId, currentEventId);
		currentTask = buildCurrentTask();
		Task tallestTask = gson.addSubtask(currentTask, parent, currentEventId);
		gson.addTaskToEvent(tallestTask, currentEvent);
		
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_TASK_ID", -1);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_PARENT_ID", currentTask.getId());
		startActivity(intent);
	}
	public void deleteCurrentTask(View view){
		Intent intent = new Intent(this, EventViewerActivity.class);
		gson.removeTask(currentTaskId, currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_PARENT_ID", currentParentId);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		startActivity(intent);
//		Intent intent = new Intent(this, )
	}

	private Task buildCurrentTask(){
		Task temp = new Task();
		
		if (currentTaskId == -1){
			currentTaskId = currentEvent.incrementTaskCount();
		}
		temp.setId(currentTaskId);
		temp.setName(((TextView) findViewById(R.id.taskManager_field_taskName)).getText().toString());
		temp.setDescription(((TextView) findViewById(R.id.taskManager_field_description)).getText().toString());
		long date = Long.parseLong(((TextView) findViewById(R.id.editEvent_hidden_dateInMillis)).getText().toString());
		long time = Long.parseLong(((TextView) findViewById(R.id.editEvent_hidden_timeInMillis)).getText().toString());
		temp.setDeadline(date + time);
		temp.setSubtasks(currentSubtasks);
		return temp;
	}
}

