package capstone.android.application.nalg.fragment;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;
import capstone.android.application.nalg.R;

public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH)+1;
		

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar tempCal = Calendar.getInstance();
        String stringDate = new String(new StringBuilder().append(month + 1)
                .append("/").append(day).append("/").append(year)
                .append(" "));
        
        final TextView newEventDate =
        			(TextView)getActivity().findViewById(R.id.editNewEventDate);
        
        tempCal.set(year, month, day, 0, 0, 0);
        
        long dateInMillis = tempCal.getTimeInMillis();
     
        newEventDate.setText(stringDate);
        ((TextView) getActivity().findViewById(R.id.editEvent_hidden_dateInMillis))
        	.setText(String.valueOf(dateInMillis));
//        Toast.makeText(getActivity(), String.valueOf(dateInMillis), Toast.LENGTH_LONG).show();
        
	}

}