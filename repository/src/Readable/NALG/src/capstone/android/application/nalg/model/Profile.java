package capstone.android.application.nalg.model;

import java.util.ArrayList;

public class Profile {
	private String id;
	
	private String name;
	private int zipcode;
	private String bio;
	private long phoneNumber;
	private String corporation;
	private ArrayList<Event> events;
	
	
	
	public Profile(String userId) {
		this.id = userId;
	}
	public ArrayList<Event> getEvents(){
		return events;
	}
	public void setEvents(ArrayList<Event> events){
		this.events = events;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int i) {
		this.zipcode = i;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long l) {
		this.phoneNumber = l;
	}
	public String getCorporation() {
		return corporation;
	}
	public void setCorporation(String corporation) {
		this.corporation = corporation;
	}
	
	
}
