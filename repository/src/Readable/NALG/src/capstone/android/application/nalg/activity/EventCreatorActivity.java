package capstone.android.application.nalg.activity;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.fragment.DatePickerFragment;
import capstone.android.application.nalg.fragment.TimePickerFragment;
import capstone.android.application.nalg.model.Event;

public class EventCreatorActivity extends Activity{
	
//	RuntimeExceptionDao<Event, Integer> eventsDao; 
	Event event;
	GsonHandler ghandler;
	String androidId = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_creator);
		setRequestedOrientation(1);
		androidId = getIntent().getExtras().getString("CURRENT_USER_ID");
		EditText dateView = ((EditText)findViewById(R.id.editNewEventDate));
		dateView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showDateDialog(v);
			}
		});
		EditText timeView = ((EditText)findViewById(R.id.editNewEventTime));
		timeView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTimeDialog(v);
			}
		});
		
	}

	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
	    	Intent intent = new Intent(this, DashboardActivity.class);
	    	intent.putExtra("CURRENT_USER_ID", androidId);
			startActivity(intent);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_creator, menu);
		return true;
	}
	
	
	public void saveEvent(View view) 
	{
		Log.i(ACTIVITY_SERVICE, "-------------------------------started-to-save-the-event");
		Intent intent = new Intent(this, EventViewerActivity.class);
		
		ghandler = new GsonHandler(getApplicationContext());
		event = buildCurrentEvent();
		ghandler.addEvent(event);
		intent.putExtra("CURRENT_EVENT_ID", event.getId());
		intent.putExtra("CURRENT_USER_ID", androidId);
        
		startActivity(intent);
	}

	private Event buildCurrentEvent()
	{
		Event currentEvent = new Event("");
		
		Calendar calDate = Calendar.getInstance();
		final TextView sDate = (TextView) findViewById(R.id.editEvent_hidden_dateInMillis);
		String sDateString = sDate.getText().toString();
		final TextView sTime = (TextView) findViewById(R.id.editEvent_hidden_timeInMillis);
		String sTimeString = new String(new StringBuilder(sTime.getText()));
		
		
		Long selectedDate = Long.parseLong(sDateString);
		Long selectedTime = Long.parseLong(sTimeString);
		long totalDateTimeInMillis = selectedDate + selectedTime;
		
		calDate.setTimeInMillis(totalDateTimeInMillis);
		
		
		currentEvent.setName(((TextView) findViewById(R.id.editEventNameField)).getText()
				.toString());
		
		currentEvent.setDescription(((TextView) findViewById(R.id.editEventDescriptionField))
				.getText().toString());

		currentEvent.setRepoURL(((TextView) findViewById(R.id.editEvent_field_repository))
				.getText().toString());
		
		
		currentEvent.setDeadline(totalDateTimeInMillis);
		currentEvent.setId(ghandler.loadProfile(androidId).getEvents().size()+1);
		return currentEvent;
	}
	public void launchTaskManager(View view) {
		Intent intent = new Intent(this, TaskManagerActivity.class);

		ghandler = new GsonHandler(getApplicationContext());
		event = ghandler.addEvent(buildCurrentEvent());
		intent.putExtra("CURRENT_EVENT_ID", event.getId());
		intent.putExtra("CURRENT_USER_ID", androidId);
		
		startActivity(intent);
	}

	public void goHome(View view) {
		Intent intent = new Intent(this, DashboardActivity.class);
		Bundle extras = getIntent().getExtras();
		if (extras != null)
			intent.putExtra("CURRENT_USER_ID",
					extras.getString("CURRENT_USER_ID"));

		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}

	public void showDateDialog(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}
	public void showTimeDialog(View view){
		DialogFragment newFragment = new TimePickerFragment();
	    newFragment.show(getFragmentManager(), "timePicker");
	}	
}
