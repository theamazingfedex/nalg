package capstone.android.application.nalg.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.model.Event;

public class ViewCurrentEventsActivity extends ListActivity {

	private GsonHandler gson = null;
	private List<Event> currentEvents = null;
	ArrayAdapter<String> adapter = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_current_events);
		setRequestedOrientation(1);
		
		gson = new GsonHandler(getApplicationContext());
		try {
			currentEvents = gson.loadProfile().getEvents();
		} catch (Exception ex){
//			Log.e("ViewCurrentEventsActivity", ex.getMessage());
		}
		
		if (currentEvents != null){
//			ListView listView = (ListView)findViewById(R.id.currentEvents_list_curEvents);
			ArrayList<String> listItems = new ArrayList<String>();
			adapter = new ArrayAdapter<String>(	getApplicationContext(), 
												R.layout.better_list_item_1, android.R.id.text1,
												listItems
												);
			setListAdapter(adapter);
			for ( String event : getCurrentEventStrings() ){
				listItems.add(event);
			}
			adapter.notifyDataSetChanged();
			
		}
//		ListView lv = this.getListView();
		this.getListView().setOnItemLongClickListener(
				new OnItemLongClickListener(){
					ListView lv = getListView();
					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int pos, long id) {
						Intent intent = new Intent(getApplicationContext(), EventViewerActivity.class);
						
						String text = (String) lv.getItemAtPosition((int)id);
						String[] parts = text.split(":");
						int entryId = (Integer.parseInt(parts[0].toString()));
//						Toast.makeText(getApplicationContext(), entryId, Toast.LENGTH_LONG).show();
						
						intent.putExtra("CURRENT_EVENT_ID", entryId);
						
						startActivity(intent);
//						Toast.makeText(getApplicationContext(), "Pressed event: "+temp.getName(), Toast.LENGTH_LONG).show();
						return true;
					}
			
				}
		);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
	    	Intent intent = new Intent(this, DashboardActivity.class);
			startActivity(intent);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	private ArrayList<String> getCurrentEventStrings() {
		ArrayList<String> strings = new ArrayList<String>();
		
		for(Event e : currentEvents)
		{
			strings.add(e.getId()+ ": "+ e.getName() + "\n  " + e.getDeadlineString());
		}
		return strings;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_current_events, menu);
		return true;
	}

	// goHome, launchNotifications, viewEvent

	public void goHome(View view) {
		Intent intent = new Intent(this, DashboardActivity.class);

		startActivity(intent);
	}

	public void launchNotifications(View view) {
		Intent intent = new Intent(this, NotificationsMenuActivity.class);

		startActivity(intent);
	}
}
