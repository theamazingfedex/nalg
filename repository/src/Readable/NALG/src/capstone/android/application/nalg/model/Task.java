package capstone.android.application.nalg.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import capstone.android.application.nalg.helpers.MySerializer;

public class Task {
	/**
	 * 
	 * 	taskId; pk, unique, autoincrement, int
		taskName; text
		taskDescription; text
		taskDeadline; int
		taskMemberIds; blob
		taskSubtaskIds; blob
	 */
	
	private int id;
	
	private String name = null;
	
	private String description = null;
	
	private long deadline;
	
	private ArrayList<Task> subtasks = null;
	
	private ArrayList<String> subscribers = null;
	
	private boolean isComplete = false;
	
	public Task(){	}
	
		
	public void updateSubtask(Task t)
	{
		for (int i = 0; i < subtasks.size(); i++)
		{
			if (subtasks.get(i).getId() == t.getId())
			{
				subtasks.remove(i);
				break;
			}
		}
		subtasks.add(t);
	}
	
	public long getDeadline(){
		return deadline;
	}
	public void setDeadline(long deadline){
		this.deadline = deadline;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int l){
		this.id = l;
	}


	public String getDeadlineString() {
		String sDeadline = null;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(deadline);
		
		int day = cal.get(Calendar.DATE);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int hour = cal.get(Calendar.HOUR);
		int minute = cal.get(Calendar.MINUTE);
		int am_pm = cal.get(Calendar.AM_PM);
		String ampm = (am_pm == Calendar.AM) ? "a.m." : "p.m.";
		
        sDeadline = (month+1) + "/" + (day) + "/" + (year) + "\n   @" + getTimeString(hour, minute);   
        
        
		return sDeadline;
	}
	private String getTimeString(int hourOfDay, int minute){
		String min = "";
		if (minute < 10)
			min += "0"+minute;
		else 
			min = String.valueOf(minute);
		
		String ampm = "";
		if (hourOfDay < 12)
			ampm = "am";
		else
			ampm = "pm";
		
		String hour = String.valueOf(hourOfDay % 12);
		if ("0".equals(hour))
			hour = "12";
		
		String stringTime = new String(new StringBuilder(hour + ":" + min + " " + ampm));
		
		return stringTime;
	}

	public boolean isComplete() {
		return isComplete;
	}
	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}




	public ArrayList<Task> getSubtasks() {
		if (subtasks == null)
			subtasks = new ArrayList<Task>();
		return subtasks;
	}
	public void setSubtasks(ArrayList<Task> subtasks) {
		this.subtasks = subtasks;
	}
	public ArrayList<String> getSubscribers() {
		if (subscribers == null)
			subscribers = new ArrayList<String>();
		return subscribers;
	}
	public void setSubscribers(ArrayList<String> subscribers) {
		this.subscribers = subscribers;
	}
}
