package capstone.android.application.nalg.activity;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.model.Event;

public class EventViewerActivity extends Activity {
	
	private int currentEventId;
	private Event currentEvent;
	private GsonHandler gson = null; 
	private String androidId = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_viewer);
		setRequestedOrientation(1);
		
		androidId = getIntent().getExtras().getString("CURRENT_USER_ID");
		gson = new GsonHandler(getApplicationContext());
		currentEventId = getIntent().getExtras().getInt("CURRENT_EVENT_ID");
		
		currentEvent = gson.getEvent(currentEventId);
		Toast.makeText(getApplicationContext(), currentEvent.getName(), Toast.LENGTH_LONG).show();
//		currentEventId = ((ApplicationWrapper) getApplicationContext()).getCurrentEventId();
		
		
		populateEventData();
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
	    	Intent intent = new Intent(this, ViewCurrentEventsActivity.class);
	    	intent.putExtra("CURRENT_USER_ID", androidId);
			startActivity(intent);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_viewer, menu);
		return true;
	}
		
	private void populateEventData()
	{
		((TextView) findViewById(R.id.eventInfo_name)).setText(currentEvent.getName());
		((TextView) findViewById(R.id.eventInfo_description)).setText(currentEvent.getDescription());
		((TextView) findViewById(R.id.eventInfo_deadline)).setText(currentEvent.getDeadlineString());
//		((ListView) findViewById(R.id.eventInfo_tasks)).setText(currentEvent.getName());
	}
	public void editEvent(View view)
	{
		Intent intent = new Intent(this, EventEditorActivity.class);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}
	public void deleteEvent(View view){
		Intent intent = new Intent(this, ViewCurrentEventsActivity.class);
		gson.removeEvent(currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}
	public void editTasks(View view)
	{
		Intent intent = new Intent(this, EventTaskViewerActivity.class);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}
	public void goHome(View view)
	{
		Intent intent = new Intent(this, DashboardActivity.class);
		intent.putExtra("CURRENT_USER_ID", androidId);		
		startActivity(intent);
	}
}
