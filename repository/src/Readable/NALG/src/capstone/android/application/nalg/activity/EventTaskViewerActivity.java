package capstone.android.application.nalg.activity;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.model.Event;
import capstone.android.application.nalg.model.Profile;
import capstone.android.application.nalg.model.Task;

public class EventTaskViewerActivity extends ListActivity {

	private int currentEventId;
	private GsonHandler gson;
	private Event currentEvent;
	private ArrayList<Task> currentTasks;
	private ArrayAdapter<String> adapter;
	private String androidId = null;
	private Profile curUser = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_viewer);
		setRequestedOrientation(1);

		androidId = getIntent().getExtras().getString("CURRENT_USER_ID");
		currentEventId = getIntent().getExtras().getInt("CURRENT_EVENT_ID");
		gson = new GsonHandler(getApplicationContext());
		currentEvent = gson.getEvent(currentEventId);
		curUser = gson.loadProfile();
		currentTasks = currentEvent.getTasks();
		
		if (currentTasks != null){
			ArrayList<String> listItems = new ArrayList<String>();
			adapter = new ArrayAdapter<String>(	getApplicationContext(), 
												R.layout.better_list_item_1, android.R.id.text1,
												listItems
												);
			setListAdapter(adapter);
			for ( String event : getCurrentTaskStrings() ){
				listItems.add(event);
			}
			adapter.notifyDataSetChanged();
			
		}
		this.getListView().setOnItemLongClickListener(
				new OnItemLongClickListener(){
					ListView lv = getListView();
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int pos, long id) {
						Intent intent = new Intent(getApplicationContext(), TaskManagerActivity.class);
						String text = (String) lv.getItemAtPosition((int)id);
						int taskId = -1;
						try {
							taskId = Integer.parseInt(text.split(":")[0]);
							intent.putExtra("CURRENT_PARENT_ID", -1);
							intent.putExtra("CURRENT_EVENT_ID", currentEventId);
							intent.putExtra("CURRENT_TASK_ID", taskId);
						} catch (Exception ex){
							intent.putExtra("CURRENT_EVENT_ID", currentEventId);
							intent.putExtra("CURRENT_PARENT_ID", currentEventId);
						}
						
						intent.putExtra("CURRENT_USER_ID", androidId);
						if (taskId != -1)
							startActivity(intent);
//						Task temp = gson.getTask((int) id+1);
//						Toast.makeText(getApplicationContext(), "Pressed task: "+temp.getName(), Toast.LENGTH_LONG).show();
						return true;
					}
			
				}
		);
	}

	private ArrayList<String> getCurrentTaskStrings() {
		ArrayList<String> strings = new ArrayList<String>();
		String defaultEntry = "No Current Tasks";
		for(Task t : currentTasks)
		{
//			if (t.getEventId() == currentEventId)
				strings.add(t.getId() + ": " + t.getName() + "\n" + t.getDeadlineString());
		}
		if (strings.size() == 0)
			strings.add(defaultEntry);
		else {
			strings.remove(defaultEntry);
		}
		return strings;
	}

	public void addNewTask(View view){
		Intent intent = new Intent(this, TaskManagerActivity.class);
		intent.putExtra("CURRENT_TASK_ID", -1);
		intent.putExtra("CURRENT_PARENT_ID", -1);
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		startActivity(intent);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
	    	Intent intent = new Intent(this, EventViewerActivity.class);
	    	intent.putExtra("CURRENT_EVENT_ID", currentEventId);
	    	intent.putExtra("CURRENT_USER_ID", androidId);
			startActivity(intent);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_viewer, menu);
		return true;
	}

}
