package capstone.android.application.nalg.activity;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.fragment.DatePickerFragment;
import capstone.android.application.nalg.fragment.TimePickerFragment;
import capstone.android.application.nalg.model.Event;
import capstone.android.application.nalg.model.Task;

public class EventEditorActivity extends Activity {
	private int currentEventId;
	private Event currentEvent;
	private ArrayList<Task> subtasks = null;
	private GsonHandler gson = null;
	private String androidId = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_editor);
		setRequestedOrientation(1);
		
		androidId = getIntent().getExtras().getString("CURRENT_USER_ID");
		gson = new GsonHandler(getApplicationContext());
		currentEventId = getIntent().getExtras().getInt("CURRENT_EVENT_ID");
		currentEvent = gson.getEvent(currentEventId);
		subtasks = currentEvent.getTasks();
		
		populateFields(currentEvent);
		
	}

	private void populateFields(Event event) {
		((EditText)findViewById(R.id.editEventNameField)).setText(event.getName());
		((EditText)findViewById(R.id.editEventDescriptionField)).setText(event.getDescription());
		((EditText)findViewById(R.id.editEvent_hidden_dateInMillis)).setText(String.valueOf(event.getDeadline()));
//		((EditText)findViewById(R.id.editEvent_hidden_timeInMillis)).setText(String.valueOf(event.getDeadline()));
		((EditText)findViewById(R.id.editNewEventDate)).setText(String.valueOf(event.getDeadlineString()));
		((EditText)findViewById(R.id.editEvent_field_repository)).setText(event.getRepoURL());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_editor, menu);
		return true;
	}
	
	
	public void saveEvent(View view) 
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		
		gson = new GsonHandler(getApplicationContext());
		Event tempEvent = buildCurrentEvent();
		tempEvent.setId(currentEventId);
		gson.updateEvent(tempEvent);
//		gson.updateList("events.txt", tempEvent);
//		gson.addEvent(currentEvent);

		intent.putExtra("CURRENT_USER_ID", androidId);
        intent.putExtra("CURRENT_EVENT_ID", currentEvent.getId());
		startActivity(intent);
	}

	private Event buildCurrentEvent()
	{
		Event currentEvent = new Event("");
		
		Calendar calDate = Calendar.getInstance();
		final TextView sDate = (TextView) findViewById(R.id.editEvent_hidden_dateInMillis);
		String sDateString = sDate.getText().toString();
		final TextView sTime = (TextView) findViewById(R.id.editEvent_hidden_timeInMillis);
		String sTimeString = new String(new StringBuilder(sTime.getText()));
		
		
		Long selectedDate = Long.parseLong(sDateString);
		Long selectedTime = Long.parseLong(sTimeString);
		long totalDateTimeInMillis = selectedDate + selectedTime;
		
		calDate.setTimeInMillis(totalDateTimeInMillis);
		
		
		currentEvent.setName(((TextView) findViewById(R.id.editEventNameField)).getText()
				.toString());
		
		currentEvent.setDescription(((TextView) findViewById(R.id.editEventDescriptionField))
				.getText().toString());

		currentEvent.setRepoURL(((TextView) findViewById(R.id.editEvent_field_repository))
				.getText().toString());
		
		
		currentEvent.setDeadline(totalDateTimeInMillis);
		
		currentEvent.setTasks(subtasks);
		return currentEvent;
	}
	public void cancelChanges(View view)
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}
	public void editTasks(View view)
	{
		Intent intent = new Intent(this, EventTaskViewerActivity.class);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
	    	Intent intent = new Intent(this, EventViewerActivity.class);
	    	intent.putExtra("CURRENT_EVENT_ID", currentEventId);
	    	intent.putExtra("CURRENT_USER_ID", androidId);
			startActivity(intent);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	public void showDateDialog(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}
	public void showTimeDialog(View view){
		DialogFragment newFragment = new TimePickerFragment();
	    newFragment.show(getFragmentManager(), "timePicker");
	}
	
}
