package capstone.android.application.nalg.activity;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.SubtaskManagerActivity;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.model.Event;
import capstone.android.application.nalg.model.Task;

public class SubtaskViewerActivity extends ListActivity {

	GsonHandler gson = null;
	int currentParentId = -1;
	int currentTaskId = -1;
	int currentEventId = -1;
	Event currentEvent = null;
	String androidId = null;
	Task currentTask = null;
	ArrayList<Task> currentTasks = null;
	ArrayAdapter<String> adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_subtask_viewer);
		setRequestedOrientation(1);
		
		gson = new GsonHandler(getApplicationContext());
		currentParentId = getIntent().getExtras().getInt("CURRENT_PARENT_ID");
		androidId = getIntent().getExtras().getString("CURRENT_USER_ID");
		currentEventId = getIntent().getExtras().getInt("CURRENT_EVENT_ID");
		currentTaskId = getIntent().getExtras().getInt("CURRENT_TASK_ID");
		currentTask = gson.getTask(currentParentId, currentEventId);
		currentEvent = gson.getEvent(currentEventId);
		if (currentTask == null)
			currentTask = new Task();
		currentTasks = currentTask.getSubtasks();
		if (currentTasks == null)
			currentTasks = new ArrayList<Task>();
		
		if (currentTasks != null){
			ArrayList<String> listItems = new ArrayList<String>();
			adapter = new ArrayAdapter<String>(	getApplicationContext(), 
												R.layout.better_list_item_1, android.R.id.text1,
												listItems
												);
			setListAdapter(adapter);
			for ( String event : getCurrentTaskStrings() ){
				listItems.add(event);
			}
			adapter.notifyDataSetChanged();
			
		}
		this.getListView().setOnItemLongClickListener(
				new OnItemLongClickListener(){
					ListView lv = getListView();
					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int pos, long id) {
						Intent intent = new Intent(getApplicationContext(), TaskManagerActivity.class);
						String text = (String) lv.getItemAtPosition((int)id);
						String[] parts = text.split(":");
						int entryId = Integer.parseInt(parts[0].toString());
//						Toast.makeText(getApplicationContext(), entryId, Toast.LENGTH_LONG).show();
						
						intent.putExtra("CURRENT_TASK_ID", entryId);
						intent.putExtra("CURRENT_PARENT_ID", currentParentId);
						intent.putExtra("CURRENT_USER_ID", androidId);
						intent.putExtra("CURRENT_EVENT_ID", currentEventId);
						
						startActivity(intent);
//						Toast.makeText(getApplicationContext(), "Pressed event: "+temp.getName(), Toast.LENGTH_LONG).show();
						return true;
					}
			
				}
		);
	}

	private ArrayList<String> getCurrentTaskStrings() {
		ArrayList<String> strings = new ArrayList<String>();
		
		for(Task t : currentTasks)
		{
			strings.add(t.getId()+ ": "+ t.getName() + "\n" + t.getDeadlineString());
		}
		return strings;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.subtask_viewer, menu);
		return true;
	}
	
	public void viewParent(View view){
		Intent intent = null;
		if (gson.getParent(gson.getTask(currentParentId, currentEventId), currentEventId) == null){
			intent = new Intent(this, TaskManagerActivity.class);
		} else {
			intent = new Intent(this, SubtaskManagerActivity.class);
		}
		intent.putExtra("CURRENT_TASK_ID", currentParentId);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_PARENT_ID", gson.getParent(currentTask, currentEventId).getId());
		startActivity(intent);
	}
	public void goHome(View view){
		Intent intent = new Intent(this, DashboardActivity.class);
		finish();
		startActivity(intent);
	}
	public void addSubtask(View view){
		Intent intent = new Intent(this, SubtaskManagerActivity.class);
		
		intent.putExtra("CURRENT_PARENT_ID", currentTask.getId());
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_TASK_ID", -1);
		
		startActivity(intent);
	}
}
