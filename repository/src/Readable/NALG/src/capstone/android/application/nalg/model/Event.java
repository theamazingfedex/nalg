package capstone.android.application.nalg.model;

import java.util.ArrayList;
import java.util.Calendar;


public class Event {
	private int id;
	
	private String name = null;
	
	private String description = null;
	
	private long deadline;
	
	private String repoURL = null;
	
	private ArrayList<Task> tasks = null;
	
	private int taskCounter;
	
	
	public Event(String name){
//		List<Integer> tIds = new ArrayList<Integer>();
//		setTaskIdsList(tIds);
	}
	public Event(){}
	
		
	public void updateSubtask(Task t)
	{
		for (int i = 0; i < tasks.size(); i++)
		{
			if (tasks.get(i).getId() == t.getId())
			{
				tasks.remove(i);
				break;
			}
		}
		tasks.add(t);
	}
//	public List<Integer> getTaskIdsList() {
//		return (ArrayList<Integer>)MySerializer.deserializeObject(taskIds);
//	}
//	public void setTaskIdsList(List<Integer> taskIds) {
//		this.taskIds = MySerializer.serializeObject(taskIds);
//	}
//	public ArrayList<Integer> getTaskIds(){
//		ArrayList<Integer> taskList = new ArrayList<Integer>();
//		
//		for (int taskId : taskIds)
//		{
//			taskList.add(taskId);
//		}
//		
//		return taskList;
//	}
//	public void setTaskIds(ArrayList<Integer> taskIds){
//		this.taskIds = taskIds;
//	}
	public String getDeadlineString(){
		String sDeadline = null;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(deadline);
		
		int day = cal.get(Calendar.DATE);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int hour = cal.get(Calendar.HOUR);
		int minute = cal.get(Calendar.MINUTE);
		int am_pm = cal.get(Calendar.AM_PM);
		String ampm = (am_pm == Calendar.AM) ? "a.m." : "p.m.";
		
        sDeadline = (month+1) + "/" + (day) + "/" + (year) + "\n   @" + getTimeString(hour, minute);   
        
        
		return sDeadline;
	}
	private String getTimeString(int hourOfDay, int minute){
		String min = "";
		if (minute < 10)
			min += "0"+minute;
		else 
			min = String.valueOf(minute);
		
		String ampm = "";
		if (hourOfDay < 12)
			ampm = "am";
		else
			ampm = "pm";
		
		String hour = String.valueOf(hourOfDay % 12);
		if (hour.equals("0"))
			hour = "12";
		
		
		String stringTime = new String(new StringBuilder(" " + hour + ":" + min + " " + ampm));
		
		return stringTime;
	}
	public long getDeadline(){
		return this.deadline;
	}
	public void setDeadline(long deadline){
		this.deadline = deadline;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRepoURL() {
		return repoURL;
	}
	public void setRepoURL(String repoURL) {
		this.repoURL = repoURL;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int l){
		this.id = l;
	}
	public ArrayList<Task> getTasks() {
		if (tasks == null)
			tasks = new ArrayList<Task>();
		return tasks;
	}
	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}
	public int getTaskCounter() {
		return taskCounter;
	}
	public int incrementTaskCount() {
		taskCounter++;
		return taskCounter;
	}
}
