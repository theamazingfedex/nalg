package capstone.android.application.nalg.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import capstone.android.application.nalg.R;

public class TimePickerFragment extends DialogFragment implements
		TimePickerDialog.OnTimeSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		int hour = 0;
		int minute = 0;

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute,
				DateFormat.is24HourFormat(getActivity()));
	}

	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		String min = "";
		if (minute < 10)
			min += "0"+minute;
		else 
			min = String.valueOf(minute);
		
		String ampm = "";
		if (hourOfDay < 12)
			ampm = "am";
		else
			ampm = "pm";
		
		String hour = String.valueOf(hourOfDay % 12);
		if (hour.equals("0"))
			hour = "12";
		
		String stringTime = new String(new StringBuilder(hour + ":" + min + " " + ampm));
		
		final TextView newEventTime =
    			(TextView)getActivity().findViewById(R.id.editNewEventTime);
		newEventTime.setText(stringTime);
		
		long timeInMillis = (((hourOfDay * 60) + minute)*60) * 1000;
		
		((TextView) getActivity().findViewById(R.id.editEvent_hidden_timeInMillis))
    		.setText(String.valueOf(timeInMillis));
		
		Toast.makeText(getActivity(), String.valueOf(timeInMillis), Toast.LENGTH_LONG).show();
	}
}
