package capstone.android.application.nalg.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.fragment.DatePickerFragment;
import capstone.android.application.nalg.fragment.TimePickerFragment;
import capstone.android.application.nalg.model.Event;
import capstone.android.application.nalg.model.Task;

public class TaskManagerActivity extends Activity {

	int currentParentId = -1;
	int currentTaskId = -1;
	int currentEventId = -1;
	Event currentEvent = null;
	Task currentTask = null;
	GsonHandler gson = null;
	String androidId = null;
	ArrayList<Task> currentSubtasks = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_manager);
		setRequestedOrientation(1);
		
		gson = new GsonHandler(getApplicationContext());
		
		androidId = getIntent().getExtras().getString("CURRENT_USER_ID");
		currentParentId = getIntent().getExtras().getInt("CURRENT_PARENT_ID");
		currentTaskId = getIntent().getExtras().getInt("CURRENT_TASK_ID");
		currentEventId = getIntent().getExtras().getInt("CURRENT_EVENT_ID");
		currentEvent = gson.getEvent(currentEventId);
		
		if (currentTaskId == -1)
		{
			currentSubtasks = new ArrayList<Task>();
			currentTask = new Task();
		}
		else 
		{
			currentTask = gson.getTask(currentTaskId, currentEventId);
			try {
				currentSubtasks = currentTask.getSubtasks();
			} catch (Exception ex){
				currentSubtasks = new ArrayList<Task>();
			}
		}
		
		
		
		//build event task list viewer
		
		EditText dateView = ((EditText)findViewById(R.id.editNewEventDate));
		dateView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showDateDialog(v);
			}
		});
		EditText timeView = ((EditText)findViewById(R.id.editNewEventTime));
		timeView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				showTimeDialog(v);
			}
		});
		try {
			populateDataFields();
		} catch (Exception ex) {
			
		}
	}
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
//	    	Intent intent = new Intent(this, EventViewerActivity.class);
//	    	intent.putExtra("CURRENT_EVENT_ID", currentTask.getEventId());
//	    	intent.putExtra("CURRENT_USER_ID", androidId);
//			startActivity(intent);
//	        return true;
//	    }
//	    return super.onKeyDown(keyCode, event);
//	}
	
	private void populateDataFields()
	{
		//split into separate declarations for debugging
		TextView taskName = ((TextView) findViewById(R.id.taskManager_field_taskName));
		taskName.setText(
				currentTask.getName());
		((TextView) findViewById(R.id.taskManager_field_description)).setText(currentTask.getDescription());
		((TextView) findViewById(R.id.editNewEventDate)).setText(currentTask.getDeadlineString());
		((TextView) findViewById(R.id.editEvent_hidden_dateInMillis)).setText(String.valueOf(currentTask.getDeadline()));
	}
	
	public void showDateDialog(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}
	public void showTimeDialog(View view){
		DialogFragment newFragment = new TimePickerFragment();
	    newFragment.show(getFragmentManager(), "timePicker");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_manager, menu);
		return true;
	}
	public void returnToEvent(View view)
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_USER_ID", androidId);
		startActivity(intent);
	}
	public void saveTask(View view)
	{
		Intent intent = new Intent(this, EventTaskViewerActivity.class);
		
		currentTask = buildCurrentTask();
		gson.addTaskToEvent(currentTask, currentEvent);
		
//		Task superTask = gson.getParent(parent, currentEvent);
//		if (superTask != null)
//			currentParentId = superTask.getId();
//		else {
//			currentParentId = currentEventId;
//			Event e = gson.addTaskToEvent(buildCurrentTask(), currentEvent);
//			gson.updateEvent(e);
//		}
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		startActivity(intent);
	}
	public void launchSubtasks(View view){
		Intent intent = new Intent(this, SubtaskViewerActivity.class);
		
		currentTask = buildCurrentTask();
		currentEvent = gson.addTaskToEvent(currentTask, currentEvent);
		
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_TASK_ID", -1);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		intent.putExtra("CURRENT_PARENT_ID", currentTask.getId());
		startActivity(intent);
	}
	public void deleteCurrentTask(View view){
		Intent intent = new Intent(this, EventViewerActivity.class);
		try {
			gson.removeTask(currentTaskId, currentEventId);
		} catch (Exception e){}
		
		intent.putExtra("CURRENT_USER_ID", androidId);
		intent.putExtra("CURRENT_PARENT_ID", currentParentId);
		intent.putExtra("CURRENT_EVENT_ID", currentEventId);
		startActivity(intent);
//		Intent intent = new Intent(this, )
	}

	private Task buildCurrentTask(){
		Task temp = new Task();
		
		if (currentTaskId == -1){
			currentTaskId = currentEvent.incrementTaskCount();
		}
		temp.setId(currentTaskId);
		temp.setName(((TextView) findViewById(R.id.taskManager_field_taskName)).getText().toString());
		temp.setDescription(((TextView) findViewById(R.id.taskManager_field_description)).getText().toString());
		long date = Long.parseLong(((TextView) findViewById(R.id.editEvent_hidden_dateInMillis)).getText().toString());
		long time = Long.parseLong(
				((TextView) findViewById(R.id.editEvent_hidden_timeInMillis)).getText().toString()
				);
		temp.setDeadline(date + time);
		temp.setSubtasks(currentSubtasks);
		return temp;
	}
}
