package capstone.android.application.nalg.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.data.GsonHandler;
import capstone.android.application.nalg.model.Profile;

public class DashboardActivity extends Activity{

	String androidId = null;
	GsonHandler gson = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setRequestedOrientation(1);
        gson = new GsonHandler(getApplicationContext());
        androidId = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Profile curUser = gson.loadProfile(androidId);
        
//        Toast.makeText(getApplicationContext(), "ID: "+curUser.getId() + " ; " +androidId, Toast.LENGTH_LONG).show();
        
    }
    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed
	    	finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    
    public void launchCurrentEvents(View view)
    { 
    	Intent intent = new Intent(this, ViewCurrentEventsActivity.class);
    	//remember to grab the auth token and pass it along once implemented here
    	intent.putExtra("CURRENT_USER_ID", androidId);
    	startActivity(intent);
    }
    public void launchEventCreator(View view)
    {
    	Intent intent = new Intent(this, EventCreatorActivity.class);
    	intent.putExtra("CURRENT_USER_ID", androidId);
    	//put any info to be forwarded here
    	startActivity(intent);
    }
    public void launchProfileManager(View view)
    {
    	Intent intent = new Intent(this, ProfileManagerActivity.class);
    	intent.putExtra("CURRENT_USER_ID", androidId);
    	//put any info to be forwarded here
    	startActivity(intent);
    }
    public void launchNotificationsMenu(View view)
    {
    	Intent intent = new Intent(this, NotificationsMenuActivity.class);
    	intent.putExtra("CURRENT_USER_ID", androidId);
    	//put any info to be forwarded here
    	startActivity(intent);
    }
}
