package capstone.android.application.nalg.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import android.util.Log;

public class MySerializer {
	private MySerializer(){
		throw new AssertionError();
	}
	
	public static byte[] serializeObject(Object o) {
		ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();

		try {
			ObjectOutput out = new ObjectOutputStream(byteOutputStream);
			out.writeObject(o);
			out.close();

			// Get the bytes of the serialized object
			byte[] buffer = byteOutputStream.toByteArray();

			return buffer;
		} catch (IOException ex) {
			Log.e("serializeObject", "error", ex);

			return null;
		}
	}

	public static Object deserializeObject(byte[] bytes) {
		
//		byte[] bytes = null;
//		
//		if (data.getClass().equals(String.class))
//		{
//			bytes = new byte[((String) data).length()];
//			bytes = ((String) data).getBytes();
//		}
//		else if (data.getClass().equals(byte[].class))
//		{
//			bytes = (byte[]) data;
//		}
		
		try {
			ObjectInputStream in = new ObjectInputStream(
					new ByteArrayInputStream(bytes));
			Object object = in.readObject();
			in.close();

			return object;
		} catch (ClassNotFoundException cnfe) {
			Log.e("deserializeObject", "class not found error", cnfe);

			return null;
		} catch (IOException ioe) {
			Log.e("deserializeObject", "io error", ioe);

			return null;
		}
	}
}
