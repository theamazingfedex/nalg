package capstone.android.application.nalg.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import capstone.android.application.nalg.abstracted.AbstractSQLiteHelper;

public class SQLiteEventHelper extends AbstractSQLiteHelper {

	public static final String TABLE_EVENTS = "events";
	public static final String COLUMN_EVENT_ID = "_eventId";
	public static final String COLUMN_EVENT_NAME = "eventName";
	public static final String COLUMN_EVENT_DESCRIPTION = "eventDescription";
	public static final String COLUMN_EVENT_DEADLINE = "eventDeadline";
	public static final String COLUMN_EVENT_REPOSITORY = "eventRepository";
	public static final String COLUMN_EVENT_TASKLIST = "eventTasklist";

	private static final String DATABASE_NAME = "events.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_EVENTS + "(" 
				+ COLUMN_EVENT_ID + " integer primary key autoincrement, " 
				+ COLUMN_EVENT_NAME	+ " text not null, " 
				+ COLUMN_EVENT_DESCRIPTION + " text not null, " 
				+ COLUMN_EVENT_DEADLINE + " integer not null, " 
				+ COLUMN_EVENT_REPOSITORY + " text" + ", "
				+ COLUMN_EVENT_TASKLIST + " BLOB" 
			+ ");";

	public SQLiteEventHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SQLiteEventHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
		onCreate(db);
	}

	
}
