package capstone.android.application.nalg.abstracted;

import android.support.v4.app.FragmentActivity;
import android.widget.ArrayAdapter;
import capstone.android.application.nalg.model.Event;

public abstract class AbstractFragmentActivity extends FragmentActivity {
	public AbstractDataSource datasource;
	public ArrayAdapter<Event> adapter;
	
	@Override
	  protected void onResume() {
//		if (datasource != null){
//			datasource.open();
//		}
		
		
	    super.onResume();
	  }

	  @Override
	  protected void onPause() {
//		if (datasource != null)
//			datasource.close();
	    super.onPause();
	  }
}
