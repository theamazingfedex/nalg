package capstone.android.application.nalg.model;

import java.util.ArrayList;
import java.util.List;

public class Event {
	private List<Integer> taskIds = null;
	private String name = null;
	private String description = null;
	private String repoURL = null;
	private String taskList = null;
	private long id;
	private long deadline;
	
	public Event(){
		taskIds = new ArrayList<Integer>();
	};
	
		
	public List<Integer> getTaskIds() {
		return taskIds;
	}
	public void setTaskIds(List<Integer> taskIds) {
		this.taskIds = taskIds;
	}
	public long getDeadline(){
		return deadline;
	}
	public void setDeadline(long deadline){
		this.deadline = deadline;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRepoURL() {
		return repoURL;
	}
	public void setRepoURL(String repoURL) {
		this.repoURL = repoURL;
	}
	public long getId() {
		return this.id;
	}
	public void setId(long l){
		this.id = l;
	}
	public String getTaskListString(){
		return this.taskList;
	}
	public void setTaskListString(String taskList) {
		this.taskList = taskList;
	}


	
}
