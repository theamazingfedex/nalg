package capstone.android.application.nalg.helpers;

import android.app.Application;
import capstone.android.application.nalg.abstracted.AbstractDataSource;

public class ApplicationWrapper extends Application {

	private AbstractDataSource eventDataSource;
	private AbstractDataSource taskDataSource;
//	AbstractDataSource 
	
	
	
	public AbstractDataSource getEventDataSource() {
		return eventDataSource;
	}
	public void setEventDataSource(AbstractDataSource eventDataSource) {
		this.eventDataSource = eventDataSource;
	}
	public AbstractDataSource getTaskDataSource() {
		return taskDataSource;
	}
	public void setTaskDataSource(AbstractDataSource taskDataSource) {
		this.taskDataSource = taskDataSource;
	}
	
}
