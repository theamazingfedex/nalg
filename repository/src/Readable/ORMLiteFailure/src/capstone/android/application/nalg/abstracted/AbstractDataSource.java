package capstone.android.application.nalg.abstracted;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import capstone.android.application.nalg.helpers.SQLiteEventHelper;

public abstract class AbstractDataSource {

	protected SQLiteDatabase database;
	protected AbstractSQLiteHelper dbHelper;

	
	public void open() throws SQLException 
	{
		database = dbHelper.getWritableDatabase();
	}

	public void close() 
	{
		dbHelper.close();
	}
}
