package capstone.android.application.nalg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.abstracted.AbstractFragmentActivity;
import capstone.android.application.nalg.data.EventsDataSource;

public class EventEditorActivity extends AbstractFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_editor);
		setRequestedOrientation(1);
		
		datasource = new EventsDataSource(this);
		datasource.open();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_editor, menu);
		return true;
	}
	
	public void saveEvent(View view)
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		
		startActivity(intent);
	}
	public void cancelChanges(View view)
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		
		startActivity(intent);
	}
	public void editTasks(View view)
	{
		Intent intent = new Intent(this, TaskManagerActivity.class);
		
		startActivity(intent);
	}
	
}
