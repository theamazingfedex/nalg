package capstone.android.application.nalg.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import capstone.android.application.nalg.abstracted.AbstractDataSource;
import capstone.android.application.nalg.helpers.MySerializer;
import capstone.android.application.nalg.helpers.SQLiteEventHelper;
import capstone.android.application.nalg.model.Event;

public class EventsDataSource extends AbstractDataSource {

	protected String[] allColumns = { 	SQLiteEventHelper.COLUMN_EVENT_ID,
										SQLiteEventHelper.COLUMN_EVENT_NAME,
										SQLiteEventHelper.COLUMN_EVENT_DESCRIPTION,
										SQLiteEventHelper.COLUMN_EVENT_DEADLINE,
										SQLiteEventHelper.COLUMN_EVENT_REPOSITORY,
										SQLiteEventHelper.COLUMN_EVENT_TASKLIST };

	public EventsDataSource(Context context) {
		super();
		dbHelper = new SQLiteEventHelper(context);
		database = dbHelper.getWritableDatabase(); 
	}

	public Event createEvent(Event event) {
		ContentValues values = new ContentValues();
//		values.put(SQLiteEventHelper.COLUMN_EVENT_ID, event.getId());
		values.put(SQLiteEventHelper.COLUMN_EVENT_NAME, event.getName());
		values.put(SQLiteEventHelper.COLUMN_EVENT_DESCRIPTION, event.getDescription());
		values.put(SQLiteEventHelper.COLUMN_EVENT_DEADLINE, event.getDeadline());
		values.put(SQLiteEventHelper.COLUMN_EVENT_REPOSITORY, event.getRepoURL());
		values.put(SQLiteEventHelper.COLUMN_EVENT_TASKLIST, MySerializer.serializeObject(event.getTaskIds()));
		long insertId = database.insert(SQLiteEventHelper.TABLE_EVENTS, null, values);

		Cursor cursor = database.query(SQLiteEventHelper.TABLE_EVENTS,
				allColumns, SQLiteEventHelper.COLUMN_EVENT_ID + " = "
						+ insertId, null, null, null, null);
		cursor.moveToFirst();
		Event newEvent = cursorToEvent(cursor);
		cursor.close();
		return newEvent;
	}
	public Event getEvent(long eventId)
	{
		Cursor cursor = database.query(SQLiteEventHelper.TABLE_EVENTS,
				allColumns, SQLiteEventHelper.COLUMN_EVENT_ID + " = "
						+ eventId, null, null, null, null);
		cursor.moveToFirst();
		Event newEvent = cursorToEvent(cursor);
		cursor.close();
		
		return newEvent;
	}

	public void deleteEvent(Event event) {
		long id = event.getId();
		System.out.println("Event deleted with id: " + id);
		database.delete(SQLiteEventHelper.TABLE_EVENTS,
				SQLiteEventHelper.COLUMN_EVENT_ID + " = " + id, null);
	}

	public List<Event> getAllEvents() {
		List<Event> events = new ArrayList<Event>();

		Cursor cursor = database.query(SQLiteEventHelper.TABLE_EVENTS,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Event event = cursorToEvent(cursor);
			events.add(event);
			cursor.moveToNext();
		}
		cursor.close();
		return events;
	}

	private Event cursorToEvent(Cursor cursor) {
				
		int dateTime = cursor.getInt(cursor.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_DEADLINE));

		Event event = new Event();
		event.setId(cursor.getLong(cursor.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_ID)));
		event.setName(cursor.getString(cursor.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_NAME)));
		event.setDescription(cursor.getString(cursor.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_DESCRIPTION)));
		event.setDeadline(dateTime);
		event.setRepoURL(cursor.getString(cursor.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_REPOSITORY)));
		event.setTaskIds((ArrayList<Integer>)MySerializer
					.deserializeObject(cursor.getBlob(
							cursor.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_TASKLIST))));
		return event;
	}

	
}
