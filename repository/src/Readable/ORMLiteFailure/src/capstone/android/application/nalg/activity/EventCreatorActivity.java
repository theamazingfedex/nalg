package capstone.android.application.nalg.activity;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.abstracted.AbstractFragmentActivity;
import capstone.android.application.nalg.data.EventsDataSource;
import capstone.android.application.nalg.fragment.DatePickerFragment;
import capstone.android.application.nalg.fragment.TimePickerFragment;
import capstone.android.application.nalg.helpers.ApplicationWrapper;
import capstone.android.application.nalg.helpers.MySerializer;
import capstone.android.application.nalg.model.Event;

public class EventCreatorActivity extends AbstractFragmentActivity {
	
	private Event curEvent = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_creator);
		setRequestedOrientation(1);
//		this.getSharedPreferences(name, mode)
//		SharedPreferences datastorage = this.getSharedPreferences("MyDataStorage",);
		
		datasource = new EventsDataSource(getApplicationContext());
		datasource.open();

//		List<Event> values = datasource.getAllEvents();

		// use the SimpleCursorAdapter to show the
		// elements in a ListView
//		adapter = new ArrayAdapter<Event>(this,
//				android.R.layout.simple_list_item_1, values);
//		setListAdapter(adapter);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_creator, menu);
		return true;
	}
	
	@Override
	protected void onResume()
	{
		try {
			datasource = (EventsDataSource)((ApplicationWrapper)getApplicationContext()).getEventDataSource();
			datasource.open();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		super.onResume();
	}
	@Override
	protected void onPause()
	{
		if (datasource != null) {
			datasource.close();
			((ApplicationWrapper)getApplicationContext()).setEventDataSource(datasource);
		}
		super.onPause();
	}
	public void saveEvent(View view) 
	{
		Intent intent = new Intent(this, EventViewerActivity.class);
		if (datasource == null)
			datasource = new EventsDataSource(getApplicationContext());
		curEvent = ((EventsDataSource) datasource).createEvent(buildCurrentEvent());
		intent.putExtra("CURRENT_EVENT_ID", curEvent.getId());
		((ApplicationWrapper)getApplicationContext()).setEventDataSource(datasource);
		startActivity(intent);
	}
	private Event buildCurrentEvent()
	{
		Event currentEvent = new Event();
		Calendar calDate = Calendar.getInstance();
		final TextView sDate = (TextView) findViewById(R.id.newEvent_hidden_dateInMillis);
		String sDateString = sDate.getText().toString();
		final TextView sTime = (TextView) findViewById(R.id.newEvent_hidden_timeInMillis);
		String sTimeString = new String(new StringBuilder(sTime.getText()));
		
		
		Long selectedDate = Long.parseLong(sDateString);
		Long selectedTime = Long.parseLong(sTimeString);
		Toast.makeText(getApplicationContext(), "boobies are building", Toast.LENGTH_LONG).show();
		long totalDateTimeInMillis = selectedDate + selectedTime;
		
		calDate.setTimeInMillis(totalDateTimeInMillis);
		
		
		currentEvent.setName(((TextView) findViewById(R.id.eventNameField)).getText()
				.toString());
		
		currentEvent.setDescription(((TextView) findViewById(R.id.eventDescriptionField))
				.getText().toString());

		currentEvent.setRepoURL(((TextView) findViewById(R.id.newEvent_field_repository))
				.getText().toString());
		
		currentEvent.setTaskIds(new ArrayList<Integer>());
		
		currentEvent.setDeadline(totalDateTimeInMillis);
		return currentEvent;
	}
	public void launchTaskManager(View view) {
		Intent intent = new Intent(this, TaskManagerActivity.class);

		startActivity(intent);
	}

	public void goHome(View view) {
		Intent intent = new Intent(this, DashboardActivity.class);
		Bundle extras = getIntent().getExtras();
		if (extras != null)
			intent.putExtra("CURRENT_USER_ID",
					extras.getString("CURRENT_USER_ID"));

		startActivity(intent);
	}

	public void showDateDialog(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}
	public void showTimeDialog(View view){
		DialogFragment newFragment = new TimePickerFragment();
	    newFragment.show(getFragmentManager(), "timePicker");
	}
}
