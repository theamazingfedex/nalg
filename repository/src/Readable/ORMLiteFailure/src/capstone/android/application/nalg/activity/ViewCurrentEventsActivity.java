package capstone.android.application.nalg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.abstracted.AbstractFragmentActivity;
import capstone.android.application.nalg.data.EventsDataSource;

public class ViewCurrentEventsActivity extends AbstractFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_current_events);
		setRequestedOrientation(1);
		datasource = new EventsDataSource(this);
		datasource.open();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_current_events, menu);
		return true;
	}

	// goHome, launchNotifications, viewEvent

	public void goHome(View view) {
		Intent intent = new Intent(this, DashboardActivity.class);

		startActivity(intent);
	}

	public void launchNotifications(View view) {
		Intent intent = new Intent(this, NotificationsMenuActivity.class);

		startActivity(intent);
	}

	public void viewEvent(View view) {
		Intent intent = new Intent(this, EventViewerActivity.class);

		startActivity(intent);
	}
}
