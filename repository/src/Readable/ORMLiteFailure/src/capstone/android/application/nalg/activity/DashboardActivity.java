package capstone.android.application.nalg.activity;

import capstone.android.application.nalg.R;
import capstone.android.application.nalg.abstracted.AbstractFragmentActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class DashboardActivity extends AbstractFragmentActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        
        setRequestedOrientation(1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    
    public void launchCurrentEvents(View view)
    {
    	Intent intent = new Intent(this, ViewCurrentEventsActivity.class);
    	//remember to grab the auth token and pass it along once implemented here
    	startActivity(intent);
    }
    public void launchEventCreator(View view)
    {
    	Intent intent = new Intent(this, EventCreatorActivity.class);
    	//put any info to be forwarded here
    	startActivity(intent);
    }
    public void launchProfileManager(View view)
    {
    	Intent intent = new Intent(this, ProfileManagerActivity.class);
    	//put any info to be forwarded here
    	startActivity(intent);
    }
    public void launchNotificationsMenu(View view)
    {
    	Intent intent = new Intent(this, NotificationsMenuActivity.class);
    	//put any info to be forwarded here
    	startActivity(intent);
    }
}
