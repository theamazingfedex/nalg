package capstone.android.application.nalg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.abstracted.AbstractFragmentActivity;
import capstone.android.application.nalg.data.EventsDataSource;
import capstone.android.application.nalg.helpers.ApplicationWrapper;
import capstone.android.application.nalg.helpers.MySerializer;
import capstone.android.application.nalg.model.Event;

public class EventViewerActivity extends AbstractFragmentActivity {
	
	private long currentEventId;
	private Event currentEvent;
	private EventsDataSource datasource;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_viewer);
		setRequestedOrientation(1);
		
		byte[] dtasrc = (byte[]) getIntent().getExtras().get("EVENT_DATASOURCE");
		
		datasource = (EventsDataSource)((ApplicationWrapper)getApplicationContext()).getEventDataSource();
		currentEventId = getIntent().getExtras().getLong("CURRENT_EVENT_ID");
		datasource.open();
		currentEvent = datasource.getEvent(currentEventId);
		
		populateEventData();
		
//		datasource = new EventsDataSource(this);
//		datasource.open();
		
		/** 
		 * 		primary key is not unique
		 * 
		 * */
	}
	@Override
	protected void onResume()
	{
		try {
			datasource = (EventsDataSource) ((ApplicationWrapper)getApplicationContext()).getEventDataSource();
			datasource.open();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		super.onResume();
	}
	@Override
	protected void onPause()
	{
		if (datasource != null) {
			datasource.close();
			((ApplicationWrapper)getApplicationContext()).setEventDataSource(datasource);
		}
		super.onPause();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_viewer, menu);
		return true;
	}
		
	private void populateEventData()
	{
		((TextView) findViewById(R.id.eventInfo_name)).setText(currentEvent.getName());
	}
	public void editEvent(View view)
	{
		Intent intent = new Intent(this, EventEditorActivity.class);
		
		startActivity(intent);
	}
	public void editTasks(View view)
	{
		Intent intent = new Intent(this, TaskManagerActivity.class);
		
		startActivity(intent);
	}
	public void goHome(View view)
	{
		Intent intent = new Intent(this, DashboardActivity.class);
		
		startActivity(intent);
	}
}
