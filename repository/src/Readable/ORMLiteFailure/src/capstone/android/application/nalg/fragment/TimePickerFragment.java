package capstone.android.application.nalg.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import capstone.android.application.nalg.R;

public class TimePickerFragment extends DialogFragment implements
		TimePickerDialog.OnTimeSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		int hour = 12;
		int minute = 0;

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute,
				DateFormat.is24HourFormat(getActivity()));
	}

	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		String stringTime = new String(new StringBuilder(hourOfDay + ":" + minute));
		
		final TextView newEventTime =
    			(TextView)getActivity().findViewById(R.id.newEventTime);
		newEventTime.setText(stringTime);
		
		long timeInMillis = (((hourOfDay * 60) + minute)*60) * 1000;
		
		((TextView) getActivity().findViewById(R.id.newEvent_hidden_timeInMillis))
    		.setText(String.valueOf(timeInMillis));
		
		Toast.makeText(getActivity(), String.valueOf(timeInMillis), Toast.LENGTH_LONG).show();
	}
}
