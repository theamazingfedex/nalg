package capstone.android.application.nalg.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import capstone.android.application.nalg.abstracted.AbstractDataSource;
import capstone.android.application.nalg.helpers.MySerializer;
import capstone.android.application.nalg.helpers.SQLiteEventHelper;
import capstone.android.application.nalg.model.Event;

public class TaskDataSource extends AbstractDataSource {

	protected String[] allColumns = { SQLiteEventHelper.COLUMN_EVENT_ID,
			SQLiteEventHelper.COLUMN_EVENT_NAME,
			SQLiteEventHelper.COLUMN_EVENT_DESCRIPTION,
			SQLiteEventHelper.COLUMN_EVENT_DEADLINE,
			SQLiteEventHelper.COLUMN_EVENT_REPOSITORY,
			SQLiteEventHelper.COLUMN_EVENT_TASKLIST };

	public TaskDataSource(Context context) {
		super();
		dbHelper = new SQLiteEventHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public Task createEvent(Task task) {
		ContentValues values = new ContentValues();
		// values.put(SQLiteEventHelper.COLUMN_EVENT_ID, event.getId());
		values.put(SQLiteEventHelper.COLUMN_EVENT_NAME, task.getName());
		values.put(SQLiteEventHelper.COLUMN_EVENT_DESCRIPTION,
				task.getDescription());
		values.put(SQLiteEventHelper.COLUMN_EVENT_DEADLINE, task.getDeadline());
		values.put(SQLiteEventHelper.COLUMN_EVENT_REPOSITORY,
				task.getRepoURL());
		values.put(SQLiteEventHelper.COLUMN_EVENT_TASKLIST,
				MySerializer.serializeObject(task.getTaskIds()));
		
		
		long insertId = database.insert(SQLiteTaskHelper.TABLE_TASKS, null,
				values);

		Cursor cursor = database.query(SQLiteTaskHelper.TABLE_TASKS,
				allColumns, SQLiteTaskHelper.COLUMN_TASK_ID + " = "
						+ insertId, null, null, null, null);
		cursor.moveToFirst();
		Task newTask = cursorToTask(cursor);
		cursor.close();
		return newTask;
	}

	public Task getTask(long taskId) {
		Cursor cursor = database.query(SQLiteTaskHelper.TABLE_TASKS,
				allColumns,
				SQLiteEventHelper.COLUMN_TASK_ID + " = " + taskId, null,
				null, null, null);
		cursor.moveToFirst();
		Task newTask = cursorToTask(cursor);
		cursor.close();

		return newTask;
	}

	public void deleteTask(Task taskId) {
		long id = taskId.getId();
		System.out.println("Task deleted with id: " + id);
		database.delete(SQLiteTaskHelper.TABLE_TASKS,
				SQLiteEventHelper.COLUMN_TASK_ID + " = " + id, null);
	}

	public List<Task> getAllTasks() {
		List<Task> tasks = new ArrayList<Task>();

		Cursor cursor = database.query(SQLiteTaskHelper.TABLE_TASKS,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Task task = cursorToTask(cursor);
			tasks.add(task);
			cursor.moveToNext();
		}
		cursor.close();
		return tasks;
	}

	private Task cursorToTask(Cursor cursor) {

		int dateTime = cursor.getInt(cursor
				.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_DEADLINE));

		Event event = new Event();
		event.setId(cursor.getLong(cursor
				.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_ID)));
		event.setName(cursor.getString(cursor
				.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_NAME)));
		event.setDescription(cursor.getString(cursor
				.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_DESCRIPTION)));
		event.setDeadline(dateTime);
		event.setRepoURL(cursor.getString(cursor
				.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_REPOSITORY)));
		event.setTaskIds((ArrayList<Integer>) MySerializer.deserializeObject(cursor.getBlob(cursor
				.getColumnIndex(SQLiteEventHelper.COLUMN_EVENT_TASKLIST))));
		return event;
	}
}
