package capstone.android.application.nalg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import capstone.android.application.nalg.R;
import capstone.android.application.nalg.abstracted.AbstractFragmentActivity;
import capstone.android.application.nalg.data.EventsDataSource;

public class TaskManagerActivity extends AbstractFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_manager);
		setRequestedOrientation(1);
		datasource = new EventsDataSource(this);
		datasource.open();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_manager, menu);
		return true;
	}
	public void returnToEvent(View view)
	{
		Intent intent = new Intent(this, EventCreatorActivity.class);
		
		startActivity(intent);
	}
	public void updateTasks(View view)
	{
		Intent intent = new Intent(this, TaskManagerActivity.class);
		
		startActivity(intent);
	}
	public void goHome(View view)
	{
		Intent intent = new Intent(this, DashboardActivity.class);
		
		startActivity(intent);
	}
}
