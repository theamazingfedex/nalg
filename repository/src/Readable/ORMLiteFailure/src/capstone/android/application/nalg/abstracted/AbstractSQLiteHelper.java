package capstone.android.application.nalg.abstracted;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import capstone.android.application.nalg.helpers.SQLiteEventHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class AbstractSQLiteHelper extends SQLiteOpenHelper {
	
	protected String DATABASE_CREATE;
	protected String ACTIVE_TABLE;

	public AbstractSQLiteHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
	
	
	public void onCreate(SQLiteDatabase database)
	{
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		Log.w(SQLiteEventHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + ACTIVE_TABLE);
		onCreate(db);
	}
}
